# BonettoChristopher_UltraBabyCarnage

Unreal Engine 4 and C++

Unreal version:
    4.25.4

## Intro
The game is about kids brutally and hilariously fighting each other inside a playground arena, where only our main character has to survive.
The player can control the character’s arms to pick up different weapons, and make combos with their attacks.

## Implementations
I analyzed a system for managing the weapons equipped by the actors on
stage and their uses, keeping data and models separate, allowing the
team to easily create / modify new weapons and effects.
